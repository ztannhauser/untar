
#include <stdio.h>
#include <assert.h>

#include <error.h>
#include <debug.h>

#include <exec.h>

#include "defines/which/7z.h"

#include <cmdline/flags.h>

#include "7z.h"

int extract_7z(
	struct flags* flags,
	char* path)
{
	int error = 0;
	char* command[20], **arg = command;
	ENTER;
	
	*arg++ = ABS_7Z_PATH; // command
	*arg++ = "x"; // extract
	*arg++ = path; // pass file
	
	if (flags->extract_into)
		*arg++ = "-o", *arg++ = flags->extract_into;
	
	*arg++ = NULL;
	
	error = exec(flags->dry_run, command);
	
	EXIT;
	return error;
}

int list_7z(
	struct flags* flags,
	char* path)
{
	int error = 0;
	char* command[20], **arg = command;
	ENTER;
	
	*arg++ = ABS_7Z_PATH; // command
	*arg++ = "l"; // extract
	*arg++ = path; // pass file
	
	*arg++ = NULL;
	
	error = exec(flags->dry_run, command);
	
	EXIT;
	return error;
}










