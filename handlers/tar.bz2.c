
#include <stdio.h>
#include <linux/limits.h>
#include <string.h>
#include <assert.h>

#include <debug.h>

#include "defines/which/tar.h"
#include "defines/which/bzip2.h"

#include <exec.h>

#include <cmdline/flags.h>

#include "tar.bz2.h"

int extract_tar_bz2(
	struct flags* flags,
	char* path)
{
	int error = 0;
	char* command[20], **arg = command;
	ENTER;
	
	*arg++ = ABS_TAR_PATH; // command
	*arg++ = "--use-compress-program", *arg++ = ABS_BZIP2_PATH;
	*arg++ = "-x"; // extract
	*arg++ = "-f", *arg++ = path; // pass file
	
	if (flags->extract_into)
		*arg++ = "-C", *arg++ = flags->extract_into,
		*arg++ = "--strip-components=1";
	
	if (flags->verbose)
		*arg++ = "-v"; // maybe set verbose flag
	
	*arg++ = NULL;
	
	if (!error)
		error = exec(flags->dry_run, command);
	
	EXIT;
	return error;
}

int list_tar_bz2(
	struct flags* flags,
	char* path)
{
	int error = 0;
	ENTER;
	TODO;
	EXIT;
	return error;
}
























