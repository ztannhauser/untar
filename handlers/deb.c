
#include <stdio.h>
#include <assert.h>

#include "defines/which/ar.h"

#include <debug.h>
#include <exec.h>

#include <cmdline/flags.h>

#include "deb.h"

int extract_deb(
	struct flags* flags,
	char* path)
{
	int error = 0;
	char* command[20], **arg = command;
	ENTER;
	
	*arg++ = ABS_AR_PATH; // command
	*arg++ = "x"; // extract
	*arg++ = path; // pass file
	
	if (flags->extract_into)
		*arg++ = "--output", *arg++ = flags->extract_into;
	
	if (flags->verbose)
		*arg++ = "v";
	
	*arg++ = NULL;
	
	error = exec(flags->dry_run, command);
	
	EXIT;
	return error;
}

int list_deb(
	struct flags* flags,
	char* path)
{
	int error = 0;
	char* command[20], **arg = command;
	ENTER;
	
	*arg++ = ABS_AR_PATH; // command
	*arg++ = "t"; // extract
	*arg++ = path; // pass file
	
	*arg++ = NULL;
	
	error = exec(flags->dry_run, command);
	
	EXIT;
	return error;
}













