
#include <stdbool.h>

struct flags;

int extract_tar(
	struct flags* flags,
	char* path);

int list_tar(
	struct flags* flags,
	char* path);

