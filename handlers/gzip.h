
#include <stdbool.h>

struct flags;

int extract_gzip(
	struct flags* flags,
	char* path);

int list_gzip(
	struct flags* flags,
	char* path);

