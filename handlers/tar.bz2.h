
#include <stdbool.h>

struct flags;

int extract_tar_bz2(
	struct flags* flags,
	char* path);

int list_tar_bz2(
	struct flags* flags,
	char* path);

