
#include <stdio.h>
#include <assert.h>

#include <debug.h>

#include "defines/which/tar.h"
#include "defines/which/xz.h"

#include <exec.h>

#include <cmdline/flags.h>

#include "tar.xz.h"

int extract_tar_xz(
	struct flags* flags,
	char* path)
{
	int error = 0;
	char* command[20], **arg = command;
	ENTER;
	
	*arg++ = ABS_TAR_PATH; // command
	*arg++ = "--use-compress-program", *arg++ = ABS_XZ_PATH; // set archive type
	*arg++ = "-x"; // extract
	*arg++ = "-f", *arg++ = path; // pass file
	
	if (flags->extract_into)
		*arg++ = "-C", *arg++ = flags->extract_into,
		*arg++ = "--strip-components=1";
	
	if (flags->touch)
		*arg++ = "-m";
		
	if (flags->verbose)
		*arg++ = "-v"; // maybe set verbose flag
	
	*arg++ = NULL;
	
	error = exec(flags->dry_run, command);
	
	EXIT;
	return error;
}

int list_tar_xz(
	struct flags* flags,
	char* path)
{
	int error = 0;
	char* command[20], **arg = command;
	ENTER;
	
	*arg++ = ABS_TAR_PATH; // command
	*arg++ = "--use-compress-program", *arg++ = ABS_XZ_PATH; // set archive type
	*arg++ = "-t"; // list contents
	*arg++ = "-f", *arg++ = path; // pass file
	
	*arg++ = NULL;
	
	error = exec(flags->dry_run, command);
	
	EXIT;
	return error;
}













