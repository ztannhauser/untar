
#include <stdio.h>
#include <assert.h>

#include <defines/which/uncompress.h>

#include <debug.h>
#include <exec.h>

#include <cmdline/flags.h>

#include "Z.h"

int extract_Z(
	struct flags* flags,
	char* path)
{
	int error = 0;
	char* command[20], **arg = command;
	ENTER;
	
	*arg++ = ABS_UNCOMPRESS_PATH; // command
	*arg++ = "--keep"; // keep archive
	*arg++ = path; // pass file
	
	if (flags->verbose)
		*arg++ = "-v"; // maybe set verbose flag
	
	*arg++ = NULL;
	
	error = exec(flags->dry_run, command);
	
	EXIT;
	return error;
}

int list_Z(
	struct flags* flags,
	char* path)
{
	int error = 0;
	char* command[20], **arg = command;
	ENTER;
	
	*arg++ = ABS_UNCOMPRESS_PATH; // command
	*arg++ = "--list"; // keep archive
	*arg++ = path; // pass file
	
	*arg++ = NULL;
	
	error = exec(flags->dry_run, command);
	
	EXIT;
	return error;
}













