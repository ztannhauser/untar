
#include <stdbool.h>

struct flags;

int extract_tar_Z(
	struct flags* flags,
	char* path);

int list_tar_Z(
	struct flags* flags,
	char* path);

