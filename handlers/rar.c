
#include <stdio.h>
#include <assert.h>

#include <error.h>
#include <debug.h>
#include <exec.h>

#include "defines/which/unrar.h"

#include <cmdline/flags.h>

#include "rar.h"

int extract_rar(
	struct flags* flags,
	char* path)
{
	int error = 0;
	char* command[20], **arg = command;
	ENTER;
	
	*arg++ = ABS_UNRAR_PATH; // command
	*arg++ = "x";
	*arg++ = path; // pass file
	
	if (flags->extract_into)
		*arg++ = flags->extract_into;
	
	if (flags->verbose)
		*arg++ = "-v"; // maybe set verbose flag
	
	*arg++ = NULL;
	
	error = exec(flags->dry_run, command);
	
	EXIT;
	return error;
}

int list_rar(
	struct flags* flags,
	char* path)
{
	int error = 0;
	char* command[20], **arg = command;
	ENTER;
	
	*arg++ = ABS_UNRAR_PATH; // command
	*arg++ = "l";
	*arg++ = path; // pass file
	
	*arg++ = NULL;
	
	error = exec(flags->dry_run, command);
	
	EXIT;
	return error;
}









