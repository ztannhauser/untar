
#include <stdbool.h>

struct flags;

int extract_tar_lz(
	struct flags* flags,
	char* path);

int list_tar_lz(
	struct flags* flags,
	char* path);

