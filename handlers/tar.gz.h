
#include <stdbool.h>

struct flags;

int extract_tar_gz(
	struct flags* flags,
	char* path);

int list_tar_gz(
	struct flags* flags,
	char* path);

