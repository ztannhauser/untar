
#include <stdio.h>
#include <assert.h>

#include <debug.h>
#include <error.h>

#include "defines/which/unzip.h"
#include "defines/which/zipinfo.h"

#include <exec.h>

#include <cmdline/flags.h>

#include "zip.h"

int extract_zip(
	struct flags* flags,
	char* path)
{
	int error = 0;
	char* command[20], **arg = command;
	ENTER;
	
	*arg++ = ABS_UNZIP_PATH; // command
	
	if (!flags->verbose)
		*arg++ = "-qq"; // maybe set verbose flag
	
	*arg++ = path; // pass file
	
	if (flags->extract_into)
		*arg++ = "-d", *arg++ = flags->extract_into;
	
	*arg++ = NULL;
	
	error = exec(flags->dry_run, command);
	
	EXIT;
	return error;
}

int list_zip(
	struct flags* flags,
	char* path)
{
	int error = 0;
	char* command[20], **arg = command;
	ENTER;
	
	*arg++ = ABS_ZIPINFO_PATH; // command
	*arg++ = "-1"; // "ls -l" style output
	*arg++ = path; // pass file
	
	*arg++ = NULL;
	
	error = exec(flags->dry_run, command);
	
	EXIT;
	return error;
}















