
#include <stdbool.h>

struct flags;

int extract_zip(
	struct flags* flags,
	char* path);

int list_zip(
	struct flags* flags,
	char* path);

