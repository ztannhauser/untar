
#include <stdbool.h>

struct flags;

int extract_tar_xz(
	struct flags* flags,
	char* path);

int list_tar_xz(
	struct flags* flags,
	char* path);

