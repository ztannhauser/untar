
enum
{
	e_success,
	e_syscall_failed,
	e_bad_cmdline_args,
	e_out_of_memory,
	e_subcommand_failed,
	e_unknown_file_type,
};

