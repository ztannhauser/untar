
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>

#include <defines/argv0.h>

#include <error.h>

#include "exec.h"

int exec(
	bool dry_run,
	char* const* args)
{
	int error = 0;
	pid_t child = -1;
	int wstatus;
	char* const* moving, *arg;
	
	if (dry_run)
	{
		putchar('$');
		
		for (moving = args; (arg = *moving); moving++)
			if (index(arg, ' '))
				printf(" '%s'", arg);
			else
				printf(" %s", arg);
		
		putchar('\n');
	}
	else if (child = fork(), child < 0)
		fprintf(stderr, "%s: fork(): %m\n", argv0),
		error = e_syscall_failed;
	else if (child && waitpid(child, &wstatus, 0) < 0)
		fprintf(stderr, "%s: waitpid(): %m\n", argv0),
		error = e_syscall_failed;
	else if (!child && execvp(args[0], (char* const*) args) < 0)
		fprintf(stderr, "%s: execvp(): %m\n", argv0),
		error = e_syscall_failed;
	else if (wstatus)
		fprintf(stderr, "%s: '%s' returned with nonzero exit code!\n",
			argv0, args[0]),
		error = e_subcommand_failed;
	
	return error;
}











