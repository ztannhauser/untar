
tars/unrar.tar.gz: https\://www.rarlab.com/rar/unrarsrc-6.0.7.tar.gz | tars/
	ln -T $< $@

unrar/built: unrar/downloaded
	make -C unrar/src/ -j `nproc`
	touch $@

unrar/installed: unrar/built
	install -D unrar/src/unrar ${PREFIX}/bin/unrar
	touch $@


