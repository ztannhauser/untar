
tars/lzip.tar.gz: https\://download.savannah.gnu.org/releases/lzip/lzip-1.22.tar.gz | tars/
	ln -T $< $@

lzip/configured: lzip/downloaded | lzip/build/
	cd $|; ../src/configure --prefix=${PREFIX}
	touch $@

lzip/built: lzip/configured
	make -C lzip/build/ -j `nproc`
	touch $@

lzip/installed: lzip/built
	make -C lzip/build/ install
	touch $@


