
#tars/libtar.tar.gz: URL=https://repo.or.cz/libtar.git/snapshot/6d0ab4c78e7a8305c36a0c3d63fd25cd1493de65.tar.gz
#tars/libtar.tar.gz: | tars/
#	wget -nc ${URL} -O $@

libtar/autoconfigured: libtar/downloaded \
	autoconf/installed automake/installed libtool/installed
	cd libtar/src; autoreconf --force --install
	touch $@

libtar/configured: libtar/autoconfigured | libtar/build/
	cd $|; ../src/configure --prefix=${PREFIX}
	touch $@

libtar/built: libtar/configured
	make -C libtar/build/ -j `nproc`
	touch $@

libtar/installed: libtar/built
	make -C libtar/build/ install
	touch $@


