
tars/7z.tar.bz2: https\://phoenixnap.dl.sourceforge.net/project/p7zip/p7zip/16.02/p7zip_16.02_src_all.tar.bz2 | tars/
	ln -T $< $@

7z/patched: 7z/downloaded
	patch -p0 < 7z.patch
	touch $@

7z/built: 7z/patched
	make -C 7z/src 7z -j `nproc`
	touch $@

7z/installed: 7z/built
	cd 7z/src; PREFIX=${PREFIX} ./install.sh
	touch $@


