
patches/unzip.patch: https\://www.linuxfromscratch.org/patches/blfs/svn/unzip-6.0-consolidated_fixes-1.patch | patches/
	ln -T $< $@

tars/unzip.tar.gz: https\://downloads.sourceforge.net/infozip/unzip60.tar.gz | tars/
	ln -T $< $@

unzip/patched: patches/unzip.patch unzip/downloaded
	patch -d unzip/src -Np1 < $<
	touch $@

unzip/built: unzip/patched
	make -C unzip/src -f unix/Makefile generic -j `nproc`
	touch $@

unzip/installed: unzip/built
	make -C unzip/src -f unix/Makefile prefix=${PREFIX} install
	touch $@


