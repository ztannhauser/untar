
tars/bzip2.tar.gz: https\://www.sourceware.org/pub/bzip2/bzip2-1.0.8.tar.gz | tars/
	ln -T $< $@

bzip2/built: bzip2/downloaded
	make -C bzip2/src clean
	make -C bzip2/src -f Makefile-libbz2_so
	make -C bzip2/src clean
	make -C bzip2/src
	touch $@

bzip2/installed: bzip2/built
	make -C bzip2/src PREFIX=${PREFIX} install
	cp -av bzip2/src/libbz2.so* ${PREFIX}/lib
	ln -svf libbz2.so.1.0 ${PREFIX}/lib/libbz2.so
	touch $@


