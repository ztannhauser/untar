
tars/xz.tar.gz: https\://tukaani.org/xz/xz-5.2.5.tar.gz | tars/
	ln -T $< $@

xz/configured: xz/downloaded | xz/build/
	cd $|; ../src/configure --prefix=${PREFIX}
	touch $@

xz/built: xz/configured
	make -C xz/build/ -j `nproc`
	touch $@

xz/installed: xz/built
	make -C xz/build/ install
	touch $@


