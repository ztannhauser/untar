
#include <stdio.h>
#include <unistd.h>
#include <stdio.h>
#include <fnmatch.h>

#include <debug.h>
#include <error.h>

#include <cmdline/flags.h>
#include <cmdline/process.h>
#include <cmdline/free.h>

#include <defines/argv0.h>

#include <macros/N.h>

#include <handlers/7z.h>
#include <handlers/a.h>
#include <handlers/deb.h>
#include <handlers/gzip.h>
#include <handlers/rar.h>
#include <handlers/tar.h>
#include <handlers/tar.xz.h>
#include <handlers/tar.gz.h>
#include <handlers/tar.bz2.h>
#include <handlers/tar.lz.h>
#include <handlers/tar.Z.h>
#include <handlers/zip.h>
#include <handlers/Z.h>

#if DEBUGGING
int debug_depth;
#endif

static const struct {
	const char *pattern;
	int (*handlers[number_of_operations])(struct flags*, char*);
} lookup[] = {
	{"*.7z",      {extract_7z,      list_7z}},
	{"*.a",       {extract_a,       list_a}},
	{"*.deb",     {extract_deb,     list_deb}},
	{"*.rar",     {extract_rar,     list_rar}},
	{"*.tar",     {extract_tar,     list_tar}},
	{"*.tar.xz",  {extract_tar_xz,  list_tar_xz}},
	{"*.tar.gz",  {extract_tar_gz,  list_tar_gz}},
	{"*.gz",      {extract_gzip,    list_gzip}},
	{"*.tgz",     {extract_tar_gz,  list_tar_gz}},
	{"*.tar.bz2", {extract_tar_bz2, list_tar_bz2}},
	{"*.tbz2",    {extract_tar_bz2, list_tar_bz2}},
	{"*.tbz",     {extract_tar_bz2, list_tar_bz2}},
	{"*.tar.lz",  {extract_tar_lz,  list_tar_lz}},
	{"*.tar.Z",   {extract_tar_Z,   list_tar_Z}},
	{"*.zip",     {extract_zip,     list_zip}},
	{"*.Z",       {extract_Z,       list_Z}},
};

int find_handler(int (**retval)(struct flags*, char*), struct flags* flags, char* path)
{
	int error = 0;
	int i, n;
	
	int (*handler)(struct flags*, char*) = NULL;
	
	for (i = 0, n = N(lookup); !handler && i < n; i++)
		if (!fnmatch(lookup[i].pattern, path, FNM_CASEFOLD))
			handler = lookup[i].handlers[flags->operation];
	
	if (!handler)
		fprintf(stderr, "%s: filetype of \"%s\" is unrecognized.\n", argv0, path),
		error = e_unknown_file_type;
	else
		*retval = handler;
	
	return error;
}

int main(int argc, char* const* argv)
{
	int error = 0;
	size_t i;
	char* path;
	struct flags* flags = NULL;
	int (*handler)(struct flags*, char*);
	ENTER;
	
	error = process_flags(&flags, argc, argv);
	
	for (i = optind; !error && i < argc; i++)
		error = 0
			?: find_handler(&handler, flags, (path = argv[i]))
			?: handler(flags, path);
	
	free_cmdline_flags(flags);
	
	EXIT;
	return error;
}
















