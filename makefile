
SHELL = /bin/bash

CC = gcc

buildtype = release

CPPFLAGS += -D _GNU_SOURCE
CPPFLAGS += -I .

CFLAGS += -Wall -Werror

ifeq ($(buildtype), release)
CPPFLAGS += -D DEBUGGING=0
CFLAGS += -O2
CFLAGS += -flto
LDFLAGS += -static
else
CPPFLAGS += -D DEBUGGING=1
CFLAGS += -g
CFLAGS += -Wno-unused-variable
CFLAGS += -Wno-unused-function
CFLAGS += -Wno-unused-but-set-variable
endif

default: gen/$(buildtype)/untar

~/gen/untar: gen/release/untar
	install -D $< $@

install: ~/gen/untar

#ARGS += --list
ARGS += -d
ARGS += /tmp/bison.tar.xz
#ARGS += -C /tmp/

run: gen/$(buildtype)/untar
	$< $(ARGS)

valrun: gen/$(buildtype)/untar
	valgrind $< $(ARGS)

defines/which/:
	mkdir -p $@

defines/which/%.h: | defines/which/
	which $* | read
	x=$*; echo "#define ABS_$${x^^}_PATH \"`which $*`\"" > $@

gen/:
	for f in $$(find -mount -type d); \
	do mkdir -p gen/debug/$$f gen/release/$$f; done

gen/srclist.mk: | gen/
	find -name '*.c' -! -path './external/*' | sed 's/^/srcs += /' > $@

ifneq "$(MAKECMDGOALS)" "clean"
include gen/srclist.mk
endif

objs = $(patsubst %.c,gen/$(buildtype)/%.o,$(srcs))
deps = $(patsubst %.c,gen/$(buildtype)/%.d,$(srcs))

gen/$(buildtype)/untar: $(objs)
	$(CC) $(LDFLAGS) $^ $(LOADLIBES) $(LDLIBS) -o $@

gen/$(buildtype)/%.d: %.c
	$(CPP) $(CPPFLAGS) $< -MM -MG -MT $@ -MF $@

gen/$(buildtype)/%.o: %.c gen/$(buildtype)/%.d
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@

clean:
	for l in $$(cat .gitignore); do rm -rvf $$l; done

ifneq "$(MAKECMDGOALS)" "clean"
include $(deps)
endif
















