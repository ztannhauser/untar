
#include <getopt.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>

#include <debug.h>
#include <error.h>

#include <memory/smalloc.h>

#include "flags.h"
#include "usage.h"
#include "process.h"

int process_flags(struct flags** retval, int argc, char* const* argv)
{
	int error = 0;
	enum operation operation = o_extract;
	bool dry_run = false;
	bool verbose = false;
	bool touch = false;
	char* extract_into = NULL;
	ENTER;
	
	// for getopt:
	int opt, option_index;
	const struct option long_options[] = {
		{"dry-run",            no_argument, 0, 'd'},
		{"verbose",            no_argument, 0, 'v'},
		{"touch",              no_argument, 0, 'm'},
		{"extract-into", required_argument, 0, 'C'},
		{"list",               no_argument, 0, 'l'},
		{0, 0, 0, 0}
	};
	
	while (!error && (opt = getopt_long(argc, argv, "dvmC:l", long_options, &option_index)) >= 0)
	{
		switch (opt)
		{
			case 'd':
				dry_run = true;
				break;
			
			case 'v':
				verbose = true;
				break;
			
			case 'm':
				touch = true;
				break;
			
			case 'C':
				extract_into = optarg;
				break;
			
			case 'l':
				operation = o_list;
				break;
			
			default:
				error = e_bad_cmdline_args;
				break;
		}
	}
	
	// at least one path must be specified
	if (!error && optind == argc)
		error = e_bad_cmdline_args;
	
	if (error == e_bad_cmdline_args)
		usage();
	
	struct flags* flags = NULL;
	
	if (!error)
		error = smalloc((void**) &flags, sizeof(*flags));
	
	if (!error)
	{
		flags->operation = operation;
		flags->dry_run = dry_run;
		flags->verbose = verbose;
		flags->touch = touch;
		flags->extract_into = extract_into;
		
		*retval = flags, flags = NULL;
	}
	
	free(flags);
	
	EXIT;
	return error;
}



















