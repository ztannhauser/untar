
#include <stdbool.h>

struct flags
{
	bool verbose;
	
	enum operation {
		o_extract,
		o_list,
		number_of_operations,
	} operation;
	
	// extract options:
	bool dry_run;
	bool touch;
	char* extract_into;
};

