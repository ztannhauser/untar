
#include <stdio.h>

#include <debug.h>

#include <defines/argv0.h>

void usage()
{
	ENTER;
	
	printf("usage: %s [-dvmC:l] <paths/to/archives> ... \n", argv0);
	
	printf("-d or --dry-run\n"
		"\t" "Prints command that would have been executed by %s\n", argv0);
	
	printf("-v or --verbose\n"
		"\t" "Sets if the command to be executed would have its \n"
		"\t" "'verbosity' flag set. Typically this means printing out the \n"
		"\t" "files as they get extracted.\n");
	
	printf("-m or --touch\n"
		"\t" "Set the access and modification time of the extracted \n"
		"\t" "files to the current time.\n");
		
	printf("-C <directory> or --extract-into=<directory>\n"
		"\t" "Sets the directory that the extracted files will be \n"
		"\t" "extracted into.\n");
	
	printf("-l or --list\n"
		"\t" "Instead of extracting, print the contents of the archive.\n");
	
	EXIT;
}

