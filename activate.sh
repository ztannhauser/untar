set -v
export PATH="$PWD/external/bin${PATH:+:${PATH}}"
export MANPATH="$PWD/external/share/man${MANPATH:+:${MANPATH}}"
export C_INCLUDE_PATH="$PWD/external/include${C_INCLUDE_PATH:+:${C_INCLUDE_PATH}}"
export CPLUS_INCLUDE_PATH="$PWD/external/include${CPLUS_INCLUDE_PATH:+:${CPLUS_INCLUDE_PATH}}"
export LIBRARY_PATH="$PWD/external/lib${LIBRARY_PATH:+:${LIBRARY_PATH}}"
export LD_LIBRARY_PATH="$PWD/external/lib${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}"
export PKG_CONFIG_PATH="$PWD/external/lib/pkgconfig${PKG_CONFIG_PATH:+:${PKG_CONFIG_PATH}}"
set +v

